// Copyright 2017 The Mellium Contributors.
// Use of this source code is governed by the BSD 2-clause
// license that can be found in the LICENSE file.

//go:generate go run ../internal/genfeature -receiver "h Handler"

// Package httpauth implements XEP-0070: Verifying HTTP Requests via XMPP.
package httpauth // import "mellium.im/xmpp/httpauth"

import (
	"context"
	"encoding/xml"
	"log"
	"strings"

	"mellium.im/xmlstream"
	"mellium.im/xmpp"
	"mellium.im/xmpp/mux"
	"mellium.im/xmpp/stanza"
)

// NS is the XML namespace used by XMPP httpauth. It is provided as a convenience.
const NS = `http://jabber.org/protocol/http-auth`

// Handler responds to http auth requests.
type Handler struct{}

// Handle returns an option that registers a Handler for http auth requests.
func Handle() mux.Option {
	return mux.IQ(stanza.GetIQ, xml.Name{Local: "confirm", Space: NS}, Handler{})
}

//////////////////////////////
// Confirmation by IQ //
//////////////////////////////

// IQ is encoded as a http auth request.
type RequestIqConfirm struct {
	stanza.IQ
	Confirm ConfirmPayload
}

// HandleIQ implements mux.IQHandler.
func (h Handler) HandleIQ(iq stanza.IQ, t xmlstream.TokenReadEncoder, start *xml.StartElement) error {
	if iq.Type != stanza.GetIQ || start.Name.Local != "confirm" || start.Name.Space != NS {
		return nil
	}

	_, err := xmlstream.Copy(t, iq.Result(nil))
	return err
}

// Send sends a confirmation request to the provided JID.
func (r RequestIqConfirm) SendIqConfirm(ctx context.Context, s *xmpp.Session) error {
	// TODO: crap but it works... See how to use the mellium style
	xmlMessage, err := xml.MarshalIndent(r, "", " ")
	if err == nil {
		myString := string(xmlMessage)
		err = s.Send(ctx, xml.NewDecoder(strings.NewReader(myString)))
	}
	return err
}

// TokenReader satisfies the xmlstream.Marshaler interface.
func (iq RequestIqConfirm) TokenReader() xml.TokenReader {
	start := xml.StartElement{Name: xml.Name{Local: "confirm", Space: NS}}
	return iq.Wrap(xmlstream.Wrap(nil, start))
}

// WriteXML satisfies the xmlstream.WriterTo interface. It is like MarshalXML
// except it writes tokens to w.
func (iq RequestIqConfirm) WriteXML(w xmlstream.TokenWriter) (int, error) {
	return xmlstream.Copy(w, iq.TokenReader())
}

// MarshalXML implements xml.Marshaler.
func (iq RequestIqConfirm) MarshalXML(e *xml.Encoder, _ xml.StartElement) error {
	_, err := iq.WriteXML(e)
	if err != nil {
		return err
	}
	return e.Flush()
}

// We receive iq result / iq error, no need for UnmarshalXML I think

//////////////////////////////
// Confirmation by Messages //
//////////////////////////////

// MessageConfirm is a message stanza that contains a body and a confirm.
type RequestMessageConfirm struct {
	stanza.Message
	Body    string `xml:"body"`
	Confirm ConfirmPayload
	Thread  string `xml:"thread,omitempty"`
}

// XEP-0070: Verifying HTTP Requests via XMPP
type ConfirmPayload struct {
	XMLName xml.Name `xml:"http://jabber.org/protocol/http-auth confirm"`
	ID      string   `xml:"id,attr"`
	Method  string   `xml:"method,attr"`
	URL     string   `xml:"url,attr"`
}

// Send sends a http auth to the provided JID and blocks until a response is
// received.
func (m RequestMessageConfirm) SendMessageConfirm(s *xmpp.Session) error {
	ctx := context.TODO()

	// s.Send(ctx, tok)
	tok := 42
	log.Print(tok, ctx)
	plop := m.TokenReader()
	log.Print(plop)
	// s.Encode(ctx, plop, m)
	return nil
}

// TokenReader satisfies the xmlstream.Marshaler interface
func (m RequestMessageConfirm) TokenReader() xml.TokenReader {
	confirm := m.Confirm

	confirmXml := xmlstream.Wrap(
		xmlstream.MultiReader(
			xmlstream.Wrap(xmlstream.Token(xml.CharData(confirm.ID)), xml.StartElement{Name: xml.Name{Local: "id"}}),
			xmlstream.Wrap(xmlstream.Token(xml.CharData(confirm.Method)), xml.StartElement{Name: xml.Name{Local: "method"}}),
			xmlstream.Wrap(xmlstream.Token(xml.CharData(confirm.URL)), xml.StartElement{Name: xml.Name{Local: "url"}}),
		),
		xml.StartElement{Name: xml.Name{Local: "confirm", Space: NS}},
	)
	log.Print(confirmXml)
	// return xmlstream.Wrap(
	// 	xmlstream.MultiReader(
	// 		xmlstream.Wrap(xmlstream.Token(xml.CharData(m.Body)), xml.StartElement{Name: xml.Name{Local: "body"}}),
	// 		xmlstream.Wrap(xmlstream.Token(xml.CharData(m.Thread)), xml.StartElement{Name: xml.Name{Local: "thread"}}),
	// 		xmlstream.Wrap(xmlstream.Token(xml.CharData(confirmXml)), xml.StartElement{Name: xml.Name{Local: "confirm"}}),
	// 	),
	// 	xml.StartElement{Name: xml.Name{Local: "time", Space: NS}},
	// )
	return confirmXml
}

// // WriteXML satisfies the xmlstream.WriterTo interface. It is like MarshalXML
// // except it writes tokens to w.
// func (m RequestMessageConfirm) WriteMessageXML(w xmlstream.TokenWriter) (int, error) {
// 	return xmlstream.Copy(w, m.TokenReaderMessage())
// }

// // MarshalXML implements xml.Marshaler.
// func (m RequestMessageConfirm) Marshaler(e *xml.Encoder, _ xml.StartElement) error {
// 	_, err := m.WriteMessageXML(e)
// 	return err
// }

// TokenReader satisfies the xmlstream.Marshaler interface.
// func (m RequestMessageConfirm) TokenReaderMessage() xml.TokenReader {
// 	start := xml.StartElement{Name: xml.Name{Local: "message", Space: NS}}
// 	return m.Wrap(xmlstream.Wrap(nil, start))
// }
